module gitlab.com/fulljourney/mysqlserver

go 1.15

// replace gitlab.com/fulljourney/mysqlparser => ../mysqlparser

require (
	github.com/DataDog/datadog-go v4.0.0+incompatible // indirect
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.1
	github.com/google/uuid v1.1.1 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.1
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/opentracing-contrib/go-grpc v0.0.0-20200813121455-4a6760c71486
	github.com/opentracing/opentracing-go v1.2.0
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/pires/go-proxyproto v0.1.3
	github.com/prometheus/client_golang v1.7.1
	github.com/stretchr/testify v1.6.1
	github.com/uber/jaeger-client-go v2.25.0+incompatible
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	gitlab.com/fulljourney/mysqlparser v0.0.0-20200823204410-ae00ec0aec81
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	google.golang.org/grpc v1.31.0
	google.golang.org/protobuf v1.23.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.26.0
	gotest.tools v2.2.0+incompatible
)
