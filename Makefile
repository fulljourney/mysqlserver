# This is only needed for upgrades / merges from the Vitess project.

PROTOC_PATH=./.bin/
PROTOC_BIN:=$(PROTOC_PATH)protoc
PROTOC_BIN_VERSION:=$(lastword $(shell $(PROTOC_BIN) --version))
PROTOS_PATH=./protos/
PKG_PATH=./pkg/
PROTOBUF_PATH=$(PKG_PATH)pb/
PROTOC_CMD=$(PROTOC_BIN) -I$(PROTOS_PATH) -I$(PROTOS_PATH)include

PROTOC_VERSION=3.12.4

OS=$(shell uname -s)
ARCH=$(shell uname -m)

# This makefile will break on windows and we REALLY don't care
# All of this is for initializing gRPC stuff and doesn't concern building otherwise
ifeq ($(OS),Darwin)
	OS_NAME:=osx
else
	OS_NAME:=$(OS)
endif

.PHONY: all update-from-vitess update-protos-from-parser update-modules update-protoc protos clean

all: install_protoc-gen-go update-protoc update-from-vitess update-protos-from-parser $(PROTOC_BIN) $(GOPATH)/bin/protoc-gen-go protos update-modules clean

update-from-vitess:
	rm -fr vitess-7.0.0* *.go pkg test protos
	curl -o vitess-7.0.0.zip https://codeload.github.com/vitessio/vitess/zip/v7.0.0
	unzip vitess-7.0.0.zip
	rm vitess-7.0.0.zip

	mkdir -p pkg test protos

	cp ./vitess-7.0.0/go/mysql/*.go ./
	cp -r ./vitess-7.0.0/go/vt/env ./pkg/
	cp -r ./vitess-7.0.0/go/stats ./pkg/
	cp -r ./vitess-7.0.0/go/vt/tlstest ./pkg/
	cp -r ./vitess-7.0.0/go/netutil ./pkg/
	cp -r ./vitess-7.0.0/go/sync2 ./pkg/
	cp -r ./vitess-7.0.0/go/bucketpool ./pkg/
	cp -r ./vitess-7.0.0/go/vt/vttls ./pkg/
	cp -r ./vitess-7.0.0/go/cache ./pkg/
	cp -r ./vitess-7.0.0/go/vt/logutil ./pkg/
	cp -r ./vitess-7.0.0/go/vt/servenv ./pkg/
	cp -r ./vitess-7.0.0/go/event ./pkg/
	cp -r ./vitess-7.0.0/go/acl ./pkg/
	cp -r ./vitess-7.0.0/go/flagutil ./pkg/
	cp -r ./vitess-7.0.0/go/vt/grpccommon ./pkg/
	cp -r ./vitess-7.0.0/go/proc ./pkg/
	cp -r ./vitess-7.0.0/go/race ./pkg/
	cp -r ./vitess-7.0.0/go/trace ./pkg/
	# The following modules are also present in mysqlparser
	cp -r ./vitess-7.0.0/go/bytes2 ./pkg/
	cp -r ./vitess-7.0.0/go/hack ./pkg/
	cp -r ./vitess-7.0.0/go/sqltypes ./pkg/
	cp -r ./vitess-7.0.0/go/tb ./pkg/
	cp -r ./vitess-7.0.0/go/test/utils ./test/

	# Modules that should be imported from the mysqlparser
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/sqltypes"#"gitlab.com/fulljourney/mysqlparser/pkg/sqltypes"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/sqlparser"#"gitlab.com/fulljourney/mysqlparser"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/proto/query"#"gitlab.com/fulljourney/mysqlparser/pkg/pb/query"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/proto/topodata"#"gitlab.com/fulljourney/mysqlparser/pkg/pb/topodata"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/proto/vtrpc"#"gitlab.com/fulljourney/mysqlparser/pkg/pb/vtrpc"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/proto/vttime"#"gitlab.com/fulljourney/mysqlparser/pkg/pb/vttime"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/vterrors"#"gitlab.com/fulljourney/mysqlparser/pkg/vterrors"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/log"#"gitlab.com/fulljourney/mysqlparser/pkg/log"#g'

	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/proto/#"gitlab.com/fulljourney/mysqlserver/pkg/pb/#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/#"gitlab.com/fulljourney/mysqlserver/pkg/#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/test/utils"#"gitlab.com/fulljourney/mysqlserver/test/utils"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/#"gitlab.com/fulljourney/mysqlserver/pkg/#g'

	# Rename the package to mysqlserver
	find ./ -name "*.go" | xargs sed -i "" -e 's#package mysql#package mysqlserver#g'

	cp ./vitess-7.0.0/proto/replicationdata.proto ./protos/
	cp ./vitess-7.0.0/proto/binlogdata.proto ./protos/
	cp ./vitess-7.0.0/proto/logutil.proto ./protos/

	cd protos; \
		sed -i "" -e 's#option go_package = "vitess.io/vitess/go/vt/proto/#option go_package = "gitlab.com/fulljourney/mysqlserver/pkg/pb/#g' \
		replicationdata.proto binlogdata.proto logutil.proto

	find ./protos -name "*.proto" | xargs sed -i "" -e 's#option java_package="io.vitess.proto";##g'

# This is needed for imports while compiling the .pb.go files
# They reference the mysqlparser .pb.go and that is where Go will import from
update-protos-from-parser:
	rm -fr mysqlparser-master*
	curl -o mysqlparser-master.zip https://gitlab.com/fulljourney/mysqlparser/-/archive/master/mysqlparser-master.zip
	unzip mysqlparser-master.zip
	rm mysqlparser-master.zip

	mkdir -p protos/include

	cp ./mysqlparser-master/protos/query.proto ./protos/include/
	cp ./mysqlparser-master/protos/topodata.proto ./protos/include/
	cp ./mysqlparser-master/protos/vtrpc.proto ./protos/include/
	cp ./mysqlparser-master/protos/vttime.proto ./protos/include/

install_protoc-gen-go:
	go install github.com/golang/protobuf/protoc-gen-go

# ensure all go modules are installed and at the right version
update-modules:
	go mod tidy

# install protoc if it's missing
$(PROTOC_BIN):
	mkdir -p $(PROTOC_PATH)
	cd $(PROTOC_PATH) && \
		rm -fr * && \
		curl -L https://github.com/protocolbuffers/protobuf/releases/download/v$(PROTOC_VERSION)/protoc-$(PROTOC_VERSION)-$(OS_NAME)-$(ARCH).zip -o protoc.zip && \
		unzip protoc.zip && \
		mv bin/protoc ./ && \
		rm -fr bin && \
		rm protoc.zip
	rm -fr $(PROTOS_PATH)google

# check if protoc needs to be upgraded
update-protoc: $(PROTOC_BIN)
ifeq ($(PROTOC_BIN_VERSION), )
	# protoc should be automatically installed in the next step
else ifneq ($(PROTOC_BIN_VERSION), $(PROTOC_VERSION))
	rm -fr $(PROTOC_PATH)
	make $(PROTOC_BIN)
endif

# Create protobuf files from .proto files
PROTO_SRCS = protos/replicationdata.proto protos/binlogdata.proto protos/logutil.proto
PROTO_SRC_NAMES = $(basename $(notdir $(PROTO_SRCS)))
PROTO_GO_OUTS = $(foreach name, $(PROTO_SRC_NAMES), $(PROTOBUF_PATH)$(name)/$(name).pb.go)

# This rule rebuilds all the go files from the proto definitions for gRPC.
protos: $(PROTO_GO_OUTS)

$(PROTO_GO_OUTS): install_protoc-gen-go protos/*.proto
	mkdir -p $(PROTOBUF_PATH)$(basename $(basename $(notdir $@)))
	$(PROTOC_CMD) --go_out=plugins=grpc:$(PROTOBUF_PATH)$(basename $(basename $(notdir $@))) --go_opt=paths=source_relative protos/$(basename $(basename $(notdir $@))).proto
	goimports -w $@

clean:
	rm -fr vitess-7.0.0*
	rm -fr mysqlparser-master*
