# mysqlserver

Isolation and improvements of the MySQL server from the Vitess project.  
This library uses [mysqlparser](gitlab.com/morphar/mysqlparser).